package pl.ugor.galleryapp;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.headers().frameOptions().disable();
        http.httpBasic();
        http.authorizeRequests()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers(HttpMethod.GET, "/").permitAll()
                .antMatchers(HttpMethod.POST, "/add-gallery-for-user").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.POST, "/shared-gallery-form").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.POST, "/create-user").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.POST, "/add-image-for-gallery").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.POST, "/delete-user").permitAll()
                .antMatchers(HttpMethod.POST, "/create-admin").permitAll()
                .antMatchers(HttpMethod.GET, "/database-management").permitAll()
                .antMatchers(HttpMethod.GET, "/clear-and-create-db").permitAll()
                .antMatchers(HttpMethod.GET, "/clear-db").permitAll()
                .antMatchers(HttpMethod.GET, "/faker-users").permitAll()
                .anyRequest().authenticated();

        http.formLogin()
                .loginPage("/login").successForwardUrl("/profile").permitAll().and().logout()
                .logoutUrl("/logout").logoutSuccessUrl("/")
                .invalidateHttpSession(true)
                .clearAuthentication(true);
    }
}