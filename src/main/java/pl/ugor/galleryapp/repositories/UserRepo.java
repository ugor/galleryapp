package pl.ugor.galleryapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import pl.ugor.galleryapp.entities.User;

public interface UserRepo extends JpaRepository <User,Long> {
    User findByUsername(String username);

    @Query(value = "select count(*) from users where users.is_Admin = 0", nativeQuery = true)
    Long findUsersNonAdmin();

    @Query(value = "select count(*) from galleries_users where galleries_id = ?1", nativeQuery = true)
    Long galleryOwnersCounter(Long galleryId);

    @Modifying
    @Transactional
    @Query(value = "delete from galleries_users where users_id = ?1 and galleries_id = ?2", nativeQuery = true)
    void deleteUserGalleryConstrain(Long userId, Long galleryId);
}
