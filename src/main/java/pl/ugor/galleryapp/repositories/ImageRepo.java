package pl.ugor.galleryapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.ugor.galleryapp.entities.Gallery;
import pl.ugor.galleryapp.entities.Image;

import java.util.List;

public interface ImageRepo extends JpaRepository <Image,Long> {

    List<Image> findByGallery(Gallery gallery);

    @Query(value = "select images.id, images.name, images.gallery_id, images.data\n" +
            "from images\n" +
            "join galleries on images.gallery_id=galleries.id\n" +
            "join galleries_users on galleries.id=galleries_users.galleries_id\n" +
            "join users on galleries_users.users_id=users.id\n" +
            "where users.username = ?1", nativeQuery = true)
    List<Image> findUsersImages(String username);

}
