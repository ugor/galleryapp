package pl.ugor.galleryapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.ugor.galleryapp.entities.Gallery;
import pl.ugor.galleryapp.entities.User;

import java.util.List;

public interface GalleryRepo extends JpaRepository <Gallery,Long> {
    List<Gallery> findByUsers(User user);
    Gallery findByName(String name);

}
