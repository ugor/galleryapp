package pl.ugor.galleryapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GalleryAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(GalleryAppApplication.class, args);
	}

}

/*
v12 - added TDD for creating, finding by id, finding by username and deleting user;
v11 - about me redirected to ugor.pl;
v10 - replaced file nio on stream utils, reason - issue to find path after maven packaging; description on index page were added;
v9 - database management added for: created users, galleries and images from static sources, created up to fake 200 users, clear all db; add delete user possibility include galleries constrains;
v8 - registration secured before adding more than one admin;
v7 - secured chosen end points by spring security; secured wrong parameter; correction; changed == on equals alongside authentication; surround get() method by if with isPresent(); deleted comment; reformat code; deleted unnecessary import;
v6 - changed relations between users and galleries on many to many for adding shared gallery possibility; added thymeleaf security authentication in nav bar fragment;
v5 - add displaying images by change getData getter for returning string encoded by Base64;
v4 - add file uploading to database and file JSON response;
v3 - typos correction;
v2 - add security - only admin can see all galleries and images, user can see only own galleries and images, not logged have access denied;
v1 - admin can add users, galleries to users and images name to galleries. User can read own galleries and images name from indicate gallery or all own images. First person on site (with empty users entity in database) will be admin;
 */