package pl.ugor.galleryapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.ugor.galleryapp.entities.Gallery;
import pl.ugor.galleryapp.entities.User;
import pl.ugor.galleryapp.repositories.GalleryRepo;
import pl.ugor.galleryapp.repositories.ImageRepo;
import pl.ugor.galleryapp.repositories.UserRepo;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GalleryController {

    @Autowired
    ImageRepo imageRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    GalleryRepo galleryRepo;

    @GetMapping("/galleries")
    public String getGalleries(Model model, @RequestParam(value = "id", required = false) Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        if (currentPrincipalName.equals("anonymousUser")) {
            return "access-denied";
        } else if (userRepo.findByUsername(currentPrincipalName).isAdmin()) {

            if (id == null) {
                model.addAttribute("galleries", galleryRepo.findAll());
                return "galleries";
            } else {

                if (!galleryRepo.findById(id).isPresent()){
                    return "wrong-parameter";
                }else {
                    model.addAttribute("galleryId", id);
                    model.addAttribute("gallery", galleryRepo.findById(id).get().getName());
                    model.addAttribute("image", imageRepo.findByGallery(galleryRepo.findById(id).get()));
                }return "gallery";
            }
        } else {
            if (id == null) {
                model.addAttribute("userGalleries", galleryRepo.findByUsers(userRepo.findByUsername(currentPrincipalName)));
                return "user-galleries";
            } else {
                if (!galleryRepo.findById(id).isPresent()) {
                    return "wrong-parameter";
                }else {
                    model.addAttribute("images", imageRepo.findByGallery(galleryRepo.findById(id).get()));
                }return "user-gallery";
            }
        }
    }

    @PostMapping("/add-gallery-for-user")
    @ResponseStatus(HttpStatus.CREATED)
    public String addGallery(@ModelAttribute Gallery gallery) {
        galleryRepo.save(gallery);
        return "request-successful";
    }

    @GetMapping("/add-gallery-for-user")
    public String addGallery(Model model, @RequestParam(value = "id", required = false) Long id) {

        if (id == null||!userRepo.findById(id).isPresent()) {
            return "wrong-parameter";
        } else {
            Gallery gallery = new Gallery();
            List<User> users = new ArrayList<>();
            users.add(userRepo.findById(id).get());
            gallery.setUsers(users);
            model.addAttribute("gallery", gallery);
        }
        return "gallery-form";
    }

    @GetMapping("/shared-gallery-form")
    public String addSharedGallery(Model model) {
        model.addAttribute("users", userRepo.findAll());
        model.addAttribute("gallery", new Gallery());
        return "shared-gallery-form";
    }

    @PostMapping("/shared-gallery-form")
    public String addSharedGallery(Gallery gallery) {
        galleryRepo.save(gallery);
        return "request-successful";
    }
}