package pl.ugor.galleryapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.ugor.galleryapp.entities.Gallery;
import pl.ugor.galleryapp.repositories.GalleryRepo;
import pl.ugor.galleryapp.repositories.UserRepo;

@Controller
public class UserController {

    @Autowired
    UserRepo userRepo;

    @Autowired
    GalleryRepo galleryRepo;

    @GetMapping("/users")
    public String getUsers(Model model, @RequestParam(value = "id", required = false) Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        if (currentPrincipalName.equals("anonymousUser")) {
            return "access-denied";
        } else if (userRepo.findByUsername(currentPrincipalName).isAdmin()) {
            if (id == null) {
                model.addAttribute("users", userRepo.findAll());
                model.addAttribute("usersNonAdmin", userRepo.findUsersNonAdmin());
                model.addAttribute("usersCount", userRepo.findAll().size());
                return "users";
            } else {

                if (!userRepo.findById(id).isPresent()) {
                    return "wrong-parameter";
                } else {
                    model.addAttribute("userId", id);
                    model.addAttribute("gallery", galleryRepo.findByUsers(userRepo.findById(id).get()));
                    model.addAttribute("user", userRepo.findById(id).get().getUsername());
                    model.addAttribute("u", userRepo.findById(id).get());
                }
                return "user";
            }
        } else return "access-denied";
    }

    @PostMapping("/delete-user")
    public String deleteUser(@RequestParam(value = "id") Long id) {

        if (userRepo.findById(id).isPresent()) {
            for (Gallery g : galleryRepo.findByUsers(userRepo.findById(id).get())) {
                if (userRepo.galleryOwnersCounter(g.getId()) > 1) {
                    userRepo.deleteUserGalleryConstrain(id,g.getId());
                }
            }
            userRepo.deleteById(id);
            return "request-successful";
        } else {
            return "bad-parameter";
        }
    }
}
