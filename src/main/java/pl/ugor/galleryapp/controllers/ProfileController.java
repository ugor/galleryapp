package pl.ugor.galleryapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import pl.ugor.galleryapp.repositories.UserRepo;

@Controller
public class ProfileController {

    @Autowired
    UserRepo userRepo;

    @GetMapping("/profile")
    public String getUserContext(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("user", currentPrincipalName);
        model.addAttribute("role", userRepo.findByUsername(currentPrincipalName));
        return "profile";
    }

    @PostMapping("/profile")
    public String getUserContext() {
        return "redirect:/profile";
    }
}