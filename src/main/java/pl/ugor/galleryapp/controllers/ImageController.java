package pl.ugor.galleryapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.ugor.galleryapp.entities.Image;
import pl.ugor.galleryapp.repositories.GalleryRepo;
import pl.ugor.galleryapp.repositories.ImageRepo;
import pl.ugor.galleryapp.repositories.UserRepo;

import java.io.IOException;
import java.util.List;

@Controller
public class ImageController {

    @Autowired
    ImageRepo imageRepo;

    @Autowired
    GalleryRepo galleryRepo;

    @Autowired
    UserRepo userRepo;

    @GetMapping("/add-image-for-gallery")
    public String addImage(Model model, @RequestParam(value = "id", required = false) Long id) {

        if (!galleryRepo.findById(id).isPresent()){
            return "wrong-parameter";
        }else {
            Image image = new Image();///////////
            model.addAttribute("image", image);
            image.setGallery(galleryRepo.findById(id).get());
        }return "image-form";
    }

    @PostMapping("/add-image-for-gallery")
    @ResponseStatus(HttpStatus.CREATED)
    public String addImage(@ModelAttribute Image image, @RequestParam("file") MultipartFile file) {
        image.setName(file.getOriginalFilename());
        try {
            image.setData(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageRepo.save(image);
        return "request-successful";
    }

    @GetMapping("/images")
    public String getUserImages(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        if (currentPrincipalName.equals("anonymousUser")){
            model.addAttribute("userImages", imageRepo.findAll());
            // TODO: 23.03.2020 access denied
            return "user-images";
        }

        else if (userRepo.findByUsername(currentPrincipalName).isAdmin()) {
            model.addAttribute("userImages", imageRepo.findAll());
            return "user-images";
        } else {
            model.addAttribute("userImages", imageRepo.findUsersImages(currentPrincipalName));
            return "user-images";
        }
    }

    @GetMapping("/imagesJSON")
    @ResponseBody
    public List<Image> getUserImages() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        return imageRepo.findUsersImages(currentPrincipalName);
    }

}
