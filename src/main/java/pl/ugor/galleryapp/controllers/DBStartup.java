package pl.ugor.galleryapp.controllers;

import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;

import pl.ugor.galleryapp.entities.Gallery;
import pl.ugor.galleryapp.entities.Image;
import pl.ugor.galleryapp.entities.User;
import pl.ugor.galleryapp.repositories.GalleryRepo;
import pl.ugor.galleryapp.repositories.ImageRepo;
import pl.ugor.galleryapp.repositories.UserRepo;

import javax.transaction.Transactional;
import java.io.*;
import java.net.URISyntaxException;
import java.util.Arrays;

@Controller
public class DBStartup {

    @Autowired
    UserRepo userRepo;
    @Autowired
    GalleryRepo galleryRepo;
    @Autowired
    ImageRepo imageRepo;

    @GetMapping("/database-management")
    String dBManagement() {
        return "database-management";
    }

    @GetMapping("/clear-and-create-db")
    @Transactional
    String clearAndCreateDB(Model model) throws IOException, URISyntaxException {

        userRepo.deleteAll();

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        User admin = new User();
        admin.setUsername("admin");
        admin.setPassword(encoder.encode("admin"));
        admin.setAdmin(true);
        userRepo.save(admin);

        for (int i = 1; i <= 5; i++) {
            String username = "u" + String.valueOf(i);
            User user = new User();
            user.setUsername(username);
            user.setPassword(encoder.encode(
                    String.valueOf(i) + String.valueOf(i) + String.valueOf(i) + String.valueOf(i)));
            user.setAdmin(false);
            userRepo.save(user);
        }

        Gallery g1u1 = new Gallery();
        g1u1.setName("g1u1");
        g1u1.setUsers(Arrays.asList(userRepo.findByUsername("u1")));
        galleryRepo.save(g1u1);

        Gallery g2u1 = new Gallery();
        g2u1.setName("g2u1");
        g2u1.setUsers(Arrays.asList(userRepo.findByUsername("u1")));
        galleryRepo.save(g2u1);

        Gallery g3u1 = new Gallery();
        g3u1.setName("g3u1");
        g3u1.setUsers(Arrays.asList(userRepo.findByUsername("u1")));
        galleryRepo.save(g3u1);

        Gallery g1u2 = new Gallery();
        g1u2.setName("g1u2");
        g1u2.setUsers(Arrays.asList(userRepo.findByUsername("u2")));
        galleryRepo.save(g1u2);

        Gallery g1u3 = new Gallery();
        g1u3.setName("g1u3");
        g1u3.setUsers(Arrays.asList(userRepo.findByUsername("u3")));
        galleryRepo.save(g1u3);

        Gallery sg1u3u4 = new Gallery();
        sg1u3u4.setName("sg1u3u4");
        sg1u3u4.setUsers(Arrays.asList(userRepo.findByUsername("u3"), userRepo.findByUsername("u4")));
        galleryRepo.save(sg1u3u4);

        Gallery sg1u4u5 = new Gallery();
        sg1u4u5.setName("sg2u4u5");
        sg1u4u5.setUsers(Arrays.asList(userRepo.findByUsername("u4"), userRepo.findByUsername("u5")));
        galleryRepo.save(sg1u4u5);

        Image u1g1i1 = new Image();
        u1g1i1.setName("u1g1i1.png");
        u1g1i1.setData(StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u1g1i1.png").getInputStream()));
        u1g1i1.setGallery(galleryRepo.findByName("g1u1"));
        imageRepo.save(u1g1i1);

        Image u1g1i2 = new Image();
        u1g1i2.setName("u1g1i2.png");
        u1g1i2.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u1g1i2.png").getInputStream())));
        u1g1i2.setGallery(galleryRepo.findByName("g1u1"));
        imageRepo.save(u1g1i2);

        Image u1g2i3 = new Image();
        u1g2i3.setName("u1g2i1.png");
        u1g2i3.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u1g2i1.png").getInputStream())));
        u1g2i3.setGallery(galleryRepo.findByName("g2u1"));
        imageRepo.save(u1g2i3);

        Image u2g1i1 = new Image();
        u2g1i1.setName("u2g1i1.png");
        u2g1i1.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u2g1i1.png").getInputStream())));
        u2g1i1.setGallery(galleryRepo.findByName("g1u2"));
        imageRepo.save(u2g1i1);

        Image u2g1i2 = new Image();
        u2g1i2.setName("u2g1i2.png");
        u2g1i2.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u2g1i2.png").getInputStream())));
        u2g1i2.setGallery(galleryRepo.findByName("g1u2"));
        imageRepo.save(u2g1i2);

        Image u2g1i3 = new Image();
        u2g1i3.setName("u2g1i3.png");
        u2g1i3.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u2g1i3.png").getInputStream())));
        u2g1i3.setGallery(galleryRepo.findByName("g1u2"));
        imageRepo.save(u2g1i3);

        Image u2g1i4 = new Image();
        u2g1i4.setName("u2g1i4.png");
        u2g1i4.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u2g1i4.png").getInputStream())));
        u2g1i4.setGallery(galleryRepo.findByName("g1u2"));
        imageRepo.save(u2g1i4);

        Image u3g1i1 = new Image();
        u3g1i1.setName("u3g1i1.png");
        u3g1i1.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u3g1i1.png").getInputStream())));
        u3g1i1.setGallery(galleryRepo.findByName("g1u3"));
        imageRepo.save(u3g1i1);

        Image u3u4sg1i1 = new Image();
        u3u4sg1i1.setName("u3u4sg1i1.png");
        u3u4sg1i1.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u3u4sg1i1.png").getInputStream())));
        u3u4sg1i1.setGallery(galleryRepo.findByName("sg1u3u4"));
        imageRepo.save(u3u4sg1i1);

        Image u4u5sg2i1 = new Image();
        u4u5sg2i1.setName("u4u5sg2i1.png");
        u4u5sg2i1.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u4u5sg2i1.png").getInputStream())));
        u4u5sg2i1.setGallery(galleryRepo.findByName("sg2u4u5"));
        imageRepo.save(u4u5sg2i1);

        Image u4u5sg2i2 = new Image();
        u4u5sg2i2.setName("u4u5sg2i2.png");
        u4u5sg2i2.setData((StreamUtils.copyToByteArray(new ClassPathResource("imgStartUpDB\\u4u5sg2i2.png").getInputStream())));
        u4u5sg2i2.setGallery(galleryRepo.findByName("sg2u4u5"));
        imageRepo.save(u4u5sg2i2);

        return "request-successful";
    }

    @GetMapping("/clear-db")
    String clearDB() {
        userRepo.deleteAll();
        return "request-successful";
    }

    @GetMapping("/faker-users")
    String fakerUsers() {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        Faker faker = new Faker();
        for (int i = 0; i < 200; i++) {
            User user = new User();
            String usernameAndPassword = faker.name().firstName();
            if (userRepo.findByUsername(usernameAndPassword) == null) {
                user.setUsername(usernameAndPassword);
                user.setPassword(encoder.encode(usernameAndPassword));
                user.setAdmin(false);
                userRepo.save(user);
            }
        }

        return "request-successful";
    }

}
