package pl.ugor.galleryapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.ugor.galleryapp.entities.User;
import pl.ugor.galleryapp.repositories.UserRepo;

@Controller
public class LoginController {

    @Autowired
    UserRepo userRepo;

    @GetMapping("/login")
    String login(Model model, String error, String logout) {
        if (userRepo.findAll().isEmpty()) {
            model.addAttribute("user", new User());
            return "register-admin";
        } else {
            return "login-form";
        }
    }
}
