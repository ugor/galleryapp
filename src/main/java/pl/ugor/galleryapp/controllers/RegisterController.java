package pl.ugor.galleryapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.ugor.galleryapp.entities.User;
import pl.ugor.galleryapp.repositories.UserRepo;

@Controller
public class RegisterController {

    @Autowired
    UserRepo userRepo;

    @PostMapping("/create-admin")
    @ResponseStatus(HttpStatus.CREATED)
    public String createAdmin(@ModelAttribute User user) {
        if (!userRepo.findAll().isEmpty()){
            return "access-denied";
        } else {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            user.setPassword(encoder.encode(user.getPassword()));
            user.setAdmin(true);
            userRepo.save(user);
        }return "request-successful";
    }

    @GetMapping("/create-user")
    String createUser(Model model) {
        model.addAttribute("user", new User());
        return "create-user-form";
    }

    @PostMapping("/create-user")
    @ResponseStatus(HttpStatus.CREATED)
    public String createUser(@ModelAttribute User user) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(user.getPassword()));
        user.setAdmin(false);
        userRepo.save(user);
        return "request-successful";
    }
}
