package pl.ugor.galleryapp.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "galleries")
public class Gallery {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String name;
    @ManyToMany
    private List<User> users;
    @OneToMany(mappedBy = "gallery", cascade = CascadeType.ALL)
    private List<Image> images;
}
