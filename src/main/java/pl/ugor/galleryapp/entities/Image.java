package pl.ugor.galleryapp.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "images")
public class Image {
    @Id
    @GeneratedValue
    private long id;
    @NotNull
    private String name;
    @Lob
    private byte[] data;
    @ManyToOne
    @JoinColumn(name = "GALLERY_ID")
    private Gallery gallery;

    public String getData() throws UnsupportedEncodingException {

        byte[] encode = Base64.getEncoder().encode(data);
        return new String(encode, "UTF-8") ;
    }
}
