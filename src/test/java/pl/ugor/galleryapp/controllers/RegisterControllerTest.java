package pl.ugor.galleryapp.controllers;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import pl.ugor.galleryapp.entities.User;
import pl.ugor.galleryapp.repositories.UserRepo;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class RegisterControllerTest {

    @Autowired
    private UserRepo userRepo;

    @Test
    void createUser() {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        User user = new User();
        user.setUsername("testUser");
        user.setPassword(encoder.encode("testPass"));
        user.setAdmin(false);
        userRepo.save(user);

        User actualUser =  userRepo.findByUsername("testUser");
        Long actualUserId = actualUser.getId();

        assertEquals(actualUser.getUsername(),"testUser");
        assertFalse(userRepo.findById(actualUserId).get().isAdmin());

        userRepo.delete(user);

        assertEquals(userRepo.findById(actualUserId), Optional.empty());
        assertNull(userRepo.findByUsername("testUser"));
    }
}